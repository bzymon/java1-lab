package com.example.lab2;

import java.util.Scanner;
import java.util.Date;

/*
Autor: Szymon Wojciech Baran
Grupa: 2ID11A
Nr zespolu: 7
Laboratorium nr 2
Zadania 5 i 7-14, wykonane na przykladzie klasy Telefonu.
Zadania 1, 3, 4 oraz zad6 wykonane osobno, z powodu ze sa to zadania zapoznawcze/generujace dokumentacje.
Zadanie 2 zostalo zamieszczone w drugim pliku.
*/


class Phone {
    private String brand;
    private String model;
    private int productionYear;
    private double screenSize;
    private int counter = 0;
    private int countObj = 0;
    {
        countObj += 1;
    }
    public Phone() {
        brand = "NoBrand";
        model = "NoModel";
        productionYear = 1900;
        screenSize = 1.0;
    }

    public Phone(String brand, String model, int productionYear, double screenSize) {
        this.brand = brand;
        this.model = model;
        this.productionYear = productionYear;
        this.screenSize = screenSize;
    }

    public Date getDate() {
        Date date = new Date();
        return date;
    }

    public void insertPhone() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj nazwe marki telefonu: ");
        brand = scan.next();
        System.out.print("Podaj nazwe modelu telefonu: ");
        model = scan.next();
        System.out.print("Podaj rok produkcji telefonu: ");
        productionYear = scan.nextInt();
        if(productionYear > 2020 || productionYear < 1970) {    //wyjatek - zad9
            throw new RuntimeException("Podano niepoprawna wartosc!");
        }
        System.out.print("Podaj rozmiar ekranu telefonu: ");
        screenSize = scan.nextDouble();
        if(screenSize > 10.0 || screenSize < 0.1) {
            throw new RuntimeException("Podano niepoprawna wartosc!");
        }
    }

    public void printPhone() {
        System.out.println("Marka: " + brand);
        System.out.println("Model: " + model);
        System.out.println("Rok produkcji: " + productionYear);
        System.out.println("Rozmiar ekranu: " + screenSize);
        counter++;
    }

    public int getCounter() {
        return counter;
    }

    public int getCountObj() {
        return countObj;
    }

    public void resetPhone() {
        brand = "NoBrand";
        model = "NoModel";
        productionYear = 1900;
        screenSize = 1.0;
    }
}

public class LAB2 {
    public static void main(String[] args) {
        Phone p1 = new Phone();
        Phone p2 = new Phone("Apple", "iPhone 4", 2010, 3.5);
        Phone p3 = new Phone();

        System.out.println("Wartosci domyslne (p1): ");
        p1.printPhone();

        System.out.println("Wartosci zainicjowane (p2): ");
        p2.printPhone();

        System.out.println("Wartosci podane przez uzytkownika (p3): ");
        p3.insertPhone();   //pobranie wartosci od uzytkownika - zad8
        p3.printPhone();
        System.out.println("Ostatnia (pierwsza) modyfikacja miala miejsce: " + p3.getDate());   //wykorzystanie klasy Date - zad10

        System.out.println("Edytuj wartosci podane przez uzytkownika (p3): ");
        p3.insertPhone();
        p3.printPhone();

        System.out.println("Przywracanie telefonu (p3) do wartosci domyslnych: ");  //zad11
        p3.resetPhone();
        p3.printPhone();

        int countSum = p1.getCounter() + p2.getCounter() + p3.getCounter(); //licznik zliczajacy kazde wypisanie obiektu - zad12
        System.out.println("Aktualna wartosc licznika wprowadzonych zmian: " + countSum);

        int countObject = p1.getCountObj() + p2.getCountObj() + p3.getCountObj(); //licznik zliczajacy obiekty klasy - zad13
        System.out.println("Licznik zliczajacy liczbe obiektow klasy: " + countObject);
    }
}