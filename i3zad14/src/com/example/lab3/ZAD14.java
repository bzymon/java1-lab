package com.example.lab3;

import java.util.Scanner;

/*
Autor: Szymon Wojciech Baran
Grupa: 2ID11A
Nr zespolu: 7
Laboratorium nr 3
Zadanie 14
*/

class Kalkulator {
    private char symbol;
    private double liczba1, liczba2, wynik;

    public Kalkulator() {
        symbol = '+';
        liczba1 = 0;
        liczba2 = 0;
        wynik = 0;
    }

    public void pobierz() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Liczba 1: ");
        liczba1 = scan.nextDouble();
        System.out.print("Symbol: ");
        symbol = scan.next().charAt(0);
        System.out.print("Liczba 2: ");
        liczba2 = scan.nextDouble();
    }

    public void policz() {
        if (symbol == '+')
            wynik = liczba1 + liczba2;
        else if (symbol == '-')
            wynik = liczba1 - liczba2;
        else if (symbol == '*')
            wynik = liczba1 * liczba2;
        else if (symbol == '/')
            wynik = liczba1 / liczba2;
        else {
            System.out.println("Wprowadzono bledny symbol!");
            return;
        }
        System.out.println("Wynik to: " + wynik);
    }
}

public class ZAD14 {
    public static void main(String[] args) {
        Kalkulator k1 = new Kalkulator();
        k1.pobierz();
        k1.policz();
    }
}
