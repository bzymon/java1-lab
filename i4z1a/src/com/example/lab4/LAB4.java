package com.example.lab4;

/*
Autor: Szymon Wojciech Baran
Grupa: 2ID11A
Nr zespolu: 7
Laboratorium nr 4
Zadania od 1 do 16
*/

import java.util.Arrays;

class Klasa {
    // static {
        private int x = 0;
        private int y = 0;

    public Klasa() {    // konstruktor domyslny - bezparametrowy
            x = 0;
            y = 0;
        }

    public Klasa( int p1, int p2) {
            x = p1;
            y = p2;
        }

        public void Wypisz () {
            System.out.println("X ma wartosc " + x + " natomiast Y ma wartosc " + y);
        }

    // }
}

public class LAB4 implements Cloneable {
    public  void finalize() throws Throwable {
        System.out.println("ZADANIE 5. Finalize oraz ZADANIE 7. Reczne wywolanie Garbage Collectora.");
    }

    public static void main(String[] args) throws Throwable {
        String z8_toString;
        System.out.println("ZADANIE 1");
        Klasa k1 = new Klasa(1, 7);
        Klasa k2 = new Klasa();
        k1.Wypisz();
        k2.Wypisz();
        System.out.println("ZADANIE 2. Konstruktor bezparametrowy jest dostepny tylko wtedy, gdy klasa nie posiada innego zaimplementowanego konstruktora.");
        System.out.println("ZADANIE 3. Przetestowane, slowa kluczowe 'static' zostaly zakomentowane w kodzie. Pozostawiono inicjalizacje niestatyczna.");
        System.out.println("ZADANIE 4. W pierwszej kolejnosci sa konstruktory klas bazowych, w kolejnosci jakiej znajduja sie w sekcji dziedziczenia w deklaracji klasy pochodnej, drugie sa konstruktory obiektow skladowych klasy w kolejnosci,\n w jakiej obiekty te zostaly zadeklarowane w ciele klasy, natomiast na koncu konstruktor klasy.");
        System.out.println("ZADANIE 6. Garbage Collector wywołuje metode finalize przed usunieciem. Zarzadza on zyciem obiektow, usuwa z pamieci te, ktore sa nieuzywane.");

        LAB4 z1 = new LAB4();
        System.gc();
        System.out.println("ZADANIE 8. i ZADANIE 9. (finalize wykonane wczesniej)");
        z8_toString = k1.toString();
        System.out.println("toString: " + z8_toString);
        System.out.println("equals: " + k1.equals(k2));
        System.out.println("clone: ");
        LAB4 z2 = (LAB4)z1.clone();
        System.out.println("ZADANIE 10.");
        int[] int_arr = {1,2,3,4,5,6,7,8,9,10};
        char[] char_arr = {'a','b','c','d','e'};
        double[] double_arr = {1.2,3.4,5.6,6.7};
        boolean[] boolean_arr = {true,false};
        System.out.println(int_arr[3]);
        System.out.println(char_arr[2]);
        System.out.println(double_arr[2]);
        System.out.println(boolean_arr[1]);
        System.out.println("ZADANIE 11.");
        String[] obj_arr = new String[4];
        obj_arr[0] = "ZEROWY";
        obj_arr[1] = "PIERWSZY";
        obj_arr[2] = "DRUGI";
        obj_arr[3] = "TRZECI";
        System.out.println(obj_arr[0]);
        System.out.println(obj_arr[3]);
        System.out.println("ZADANIE 12.");
        int[] copied_int_arr = new int[int_arr.length];
        System.arraycopy(int_arr, 0, copied_int_arr, 0, int_arr.length);
        System.out.println(copied_int_arr[3]);
        System.out.println("ZADANIE 13.");
        String[] str_arr1 = {new String("JEDEN"), "DWA"};
        String[] str_arr2 = {new String("JEDEN"), "DWA"};
        String[][] str_arr3 = {str_arr1, str_arr2};
        String[][] str_arr4 = {str_arr2, str_arr1};
        System.out.println("Porownanie tablicy str_arr3 i str_arr4: " + Arrays.equals(str_arr3, str_arr4));
        System.out.println("Glebokie porownanie tych samych tablic: " + Arrays.deepEquals(str_arr3, str_arr4));
        System.out.println("ZADANIE 14.");
        for (int i=0; i<obj_arr.length; i++) {
            if(obj_arr[i] == "SIEDEM")
                System.out.println("Znaleziono SIEDEM");
            else if (obj_arr[i] == "PIERWSZY")
                System.out.println("Znaleziono PIERWSZY, na miejscu: " + i);
        }
        System.out.println("ZADANIE 15.");
        System.out.println(obj_arr[3]);
        Arrays.sort(obj_arr);
        System.out.println(obj_arr[3]);
    }

}
