package com.example.lab2;

/*
Autor: Szymon Wojciech Baran
Grupa: 2ID11A
Nr zespolu: 7
Laboratorium nr 2
Zadanie 2
*/

class Testing {
    private boolean Boolean_test;
    private char Char_test;
    private byte Byte_test;
    private short Short_test;
    private int Int_test;
    private long Long_test;
    private float Float_test;
    private double Double_test;

    public Testing() {
        Boolean_test = true;
        Char_test = 'a';
        Byte_test = 127;
        Short_test = 32767;
        Int_test = 2147483647;
        Long_test = 1728391024;
        Float_test = 3.14F;
        Double_test = 78.20319;
    }

    public void Printer() {
        System.out.println("Boolean: " + Boolean_test);
        System.out.println("Character: " + Char_test);
        System.out.println("Byte: " + Byte_test);
        System.out.println("Short: " + Short_test);
        System.out.println("Integer: " + Int_test);
        System.out.println("Long: " + Long_test);
        System.out.println("Float: " + Float_test);
        System.out.println("Double: " + Double_test);
    }
}

public class ZAD2 {
    public static void main(String[] args) {
        Testing t1 = new Testing();
        t1.Printer();
    }
}
