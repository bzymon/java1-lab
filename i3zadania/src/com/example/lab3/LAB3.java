package com.example.lab3;

import java.util.Scanner;

/*
Autor: Szymon Wojciech Baran
Grupa: 2ID11A
Nr zespolu: 7
Laboratorium nr 3
Zadania 2-13
Zadanie 14 zgodnie z poleceniem wykonano osobno
*/

class Klasa {
    private int i1;
    private String str1;
    private String str_int;
    private String str_float;
    private int i2;
    private long l1;
    private float f1;
    private double d1;


    public Klasa() {
        i1 = 17;
        str1 = "pusty";
        str_int = "29";
        str_float = "3.74";
    }

    public int getI1() {
        return i1;
    }

    public String getStr_int() {
        return str_int;
    }

    public String getStr_float() {
        return str_float;
    }

    public String int_toString(int i) {
        str1 = Integer.toString(i);
        return str1;
    }

    public int string_toInt(String s) {
        i2 = Integer.parseInt(s);
        return i2;
    }

    public long string_toLong(String s) {
        l1 = Long.parseLong(s);
        return l1;
    }

    public float string_toFloat(String s) {
        f1 = Float.parseFloat(s);
        return f1;
    }

    public double string_toDouble(String s) {
        d1 = Double.parseDouble(s);
        return d1;
    }

    public int test_IF() {
        int liczba;
        Scanner scan = new Scanner(System.in);
        System.out.print("TEST IF: Podaj liczbe 1 lub 2");
        liczba = scan.nextInt();
        if (liczba == 1) {
            System.out.println("JEDEN");
            return 1;
        }
        else if (liczba == 2) {
            System.out.println("DWA");
            return 2;
        }
        else {
            System.out.println("Podano nieobslugiwana liczbe.");
            return -1;
        }
    }

    public void test_SWITCH() {
        int liczba;
        Scanner scan = new Scanner(System.in);
        System.out.print("TEST SWITCH: Podaj liczbe 10");
        liczba = scan.nextInt();
        switch (liczba) {
            case 10:
                System.out.println("Podano 10.");
                break;
            default:
                System.out.println("Nie podano 10.");
        }
    }

    public void test_WHILE() {
        int liczba = 0;
        System.out.println("Test WHILE: Licze od 0 do 4.");
        while (liczba < 5) {
            System.out.println(liczba);
            liczba++;
        }
    }

    public void test_DOWHILE() {
        int liczba = 5;
        System.out.println("Test DO WHILE: Licze od 5 do 9");
        do {
            System.out.println(liczba);
            liczba++;
        }
        while(liczba < 10);
    }

    public void test_FOR() {
        System.out.println("Test FOR: Licze od 10 do 14");
        for (int liczba = 10; liczba < 15; liczba++)
            System.out.println(liczba);
    }

    public void test_FOREACH() {
        int[] liczby = {200,100,50,20,10};
        System.out.println("Test FOREACH:");
        for (int liczba : liczby) {
            System.out.println(liczba);
        }
    }

    public void test_WARUNK() {
        Double liczba = -5.5;
        String wynik;
        System.out.println("Test operatora trojargumentowego:");
        wynik = (liczba > 0.0) ? "dodatnia" : "ujemna";
        System.out.println(liczba + " jest " + wynik);
    }

    public void test_BR_CON() {
        petla1: for (int liczba = 0; liczba < 6; liczba++) {
            if (liczba == 4)
                break petla1;
            System.out.println(liczba);
        }
        petla2: for (int liczba = 10; liczba < 16; liczba++) {
            if (liczba == 14)
                continue petla2;
            System.out.println(liczba);
        }
    }

    public void test_MAT() {
        double liczba1 = 2.44;
        double liczba2 = 3;
        double wynik;
        wynik = (3.78 + 4)*(liczba1/2)+liczba2;
        System.out.println("Wynik to: " + wynik);
        if (wynik < 10)
            System.out.println("Jest mniejszy od 10.");
        else if (wynik >= 10) {
            System.out.println("Jest wiekszy lub rowny 10.");
            if (wynik != 11)
                System.out.println("Nie jest rowny 11.");
        }
    }

    public void test_EQL() {
        String slowo1 = new String ("TEST");
        String slowo2 = new String ("TEST");
        System.out.println(slowo1 == slowo2);
        System.out.println(slowo1.equals(slowo2));
    }

    public void test_rzut() {
        double liczba1 = 1.7;
        int liczba2 = (int)liczba1;
        float liczba3 = (float)liczba2;
        long liczba4 = (long)liczba3;
        System.out.println("Double: " + liczba1 + " Int: " + liczba2 + " Float: " + liczba3 + " Long: " + liczba4);
    }

    public boolean ret_TRUE() {
        System.out.println("Jestem ret_TRUE i ZWRACAM TRUE");
        return true;
    }

    public boolean ret_FALSE() {
        System.out.println("Jestem ret_FALSE i ZWRACAM FALSE");
        return false;
    }



}

public class LAB3 {
    public static void main(String[] args) {
        Klasa k1 = new Klasa();
        System.out.println("Zadanie 2: konwersja liczby " + k1.getI1() + " na lancuch znakow: " + k1.int_toString(k1.getI1()) );
        System.out.println("Zadanie 3: konwersja String: " + k1.getStr_int() + " na Int: " + k1.string_toInt(k1.getStr_int()) + ", nastepnie tego samego String na Long: " + k1.string_toLong(k1.getStr_int()) );
        System.out.println("Konwersja String: " + k1.getStr_float() + " na Float: " + k1.string_toFloat(k1.getStr_float()) + " oraz tego samego String na Double: " + k1.string_toDouble(k1.getStr_float()) );
        System.out.println("Zadanie 4");
        k1.test_IF();
        k1.test_SWITCH();
        k1.test_WHILE();
        k1.test_DOWHILE();
        k1.test_FOR();
        System.out.println("Zadanie 5");
        k1.test_FOREACH();
        System.out.println("Zadanie 6");
        k1.test_WARUNK();
        System.out.println("Zadanie 7 oraz 8");
        k1.test_BR_CON(); //w przypadku 'break' petla zostaje calkowicie "zastopowana", podczas gdy 'continue' omija tylko liczbe, ale kontynuuje petle
        System.out.println("Zadanie 9 oraz 10");
        k1.test_MAT();
        System.out.println("Zadanie 11");
        k1.test_EQL(); //== jest nieskuteczne dla porownywania obiektow, gdyz porownuje referencje a nie ich rownosci strukturalna
        System.out.println("Zadanie 12");
        k1.test_rzut();
        System.out.println("Zadanie 13");
        if(k1.ret_TRUE() == true)
            System.out.println("ret_TRUE() zwrocil TRUE");
        if(k1.ret_FALSE() != true)
            System.out.println("ret_FALSE() nie zwrocil TRUE");

    }
}
